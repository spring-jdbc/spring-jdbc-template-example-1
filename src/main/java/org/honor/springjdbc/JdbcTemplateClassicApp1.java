package org.honor.springjdbc;

import java.util.List;

import org.honor.springjdbc.dao.OrganizationDao;
import org.honor.springjdbc.domain.Organization;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JdbcTemplateClassicApp1 {

	public static void main(String[] args) {
		//create application context
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml");
		
		//create the bean
		//coding to interface is a bect practice
		OrganizationDao dao = (OrganizationDao) ctx.getBean("orgDaoImpl");
		
		DaoUtils.createSeedData(dao);
		
		List<Organization> orgs = dao.getAllOrganizations();
		DaoUtils.printOrganizations(orgs, DaoUtils.readOperation);
		
		Organization org = new Organization("General Electiric", 1978, "434343", 5455, "imagine");
		boolean isCreated = dao.create(org);
		DaoUtils.printSuccessFailure(DaoUtils.createOperation, isCreated);
		DaoUtils.printOrganizationCount(dao.getAllOrganizations(), DaoUtils.createOperation);
		DaoUtils.printOrganizations(dao.getAllOrganizations(), DaoUtils.createOperation);
		
		Organization org2 = dao.getOrganization(1);
		DaoUtils.printOrganization(org2, "getOrganization");
		
		Organization org3 = dao.getOrganization(2);
		org3.setSlogan("we imagine we do...");
		boolean isUpdated = dao.update(org3);
		
		boolean isDeleted = dao.delete(dao.getOrganization(3));
		DaoUtils.printSuccessFailure(DaoUtils.deleteOperation, isDeleted);
		DaoUtils.printOrganizations(dao.getAllOrganizations(), DaoUtils.deleteOperation);
		
		DaoUtils.printSuccessFailure(DaoUtils.updateOperation, isUpdated);
		DaoUtils.printOrganization(dao.getOrganization(2), DaoUtils.updateOperation);
		
		dao.cleanup();
		DaoUtils.printOrganizationCount(dao.getAllOrganizations(), DaoUtils.cleanupOperation);
		
		((ClassPathXmlApplicationContext) ctx).close();
	}

}
